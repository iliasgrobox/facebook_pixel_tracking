<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking_Public {

    // Override Cartflows Facebook pixel settings
    public function override_cartflows_facebook_settings( $localize ) {

        global $post;

        $tracking_id = (int) get_post_meta( $post->ID, 'cartflows_fb_tracking_flow_tracking', true );

        // Override default Facebook pixel tracking ID if marketer filled their custom tracking ID
        if ( isset( $localize['fb_active']['facebook_pixel_id'] ) ) {
            $localize['fb_active']['facebook_pixel_id'] = $tracking_id ?: $localize['fb_active']['facebook_pixel_id'];
        }

        return $localize;

    }

    // Print Facebook pixel tracking script
    public function print_fb_tracking_script() {

        global $post;

        // Get tracking ID and event for specified flow and step
        $tracking_id = (int) get_post_meta( $post->ID, 'cartflows_fb_tracking_flow_tracking', true );
        $event       = get_post_meta( $post->ID, 'cartflows_fb_tracking_event', true );

        // Check if Cartflows FB pixel settings is disable
        $is_enable_cf_fb_pixel = isset( get_option( '_cartflows_facebook' )['facebook_pixel_tracking'] )
                                    ? get_option( '_cartflows_facebook' )['facebook_pixel_tracking'] == 'enable'
                                    : false;

        // Make sure tracking ID and event is not empty, and Cartflows FB pixel settings is enable
        if ( empty( $tracking_id ) || empty( $event ) || !$is_enable_cf_fb_pixel ) {
            return;
        }
        ?>
        <!-- Facebook Pixel Code -->
        <script>
        fbq('init', '<?php echo $tracking_id; ?>');
        fbq('track', '<?php echo $event; ?>');
        </script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=<?php echo $tracking_id; ?>&ev=<?php echo $event; ?>&noscri
        pt=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>

            var isActive = 'start';

            function facebook_pixel_events(event_name=null)
            {
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '<?php echo $tracking_id; ?>');
                fbq('track', event_name);
            }

            function find_suku() 
            {

                var available = $(document).height();
                var percentage_of_page = 0.15;
                var suku_screen = available * percentage_of_page;

                return suku_screen;

            }

            function find_half() 
            {

                var available = $(document).height();
                var percentage_of_page = 0.5;
                var half_screen = available * percentage_of_page;

                return half_screen;

            }

            function find_bottom() 
            {

                var available = $(document).height();
                var percentage_of_page = 0.8;
                var bottom_screen = available * percentage_of_page;

                return bottom_screen;

            }

            $(window).ready(function (e) 
            {
                setTimeout(function()
                {
                    var event_name = 'Twenty_Secs_View';
                    facebook_pixel_events(event_name);
                }, 
                20000);
            } 
            );

            $(window).scroll(function (e) 
            {
                var available = $(document).height();

                var height = $(window).scrollTop();

                var suku_screen = find_suku();

                var half_screen = find_half();

                var bottom_screen = find_bottom();

                if (height >= suku_screen  && isActive == 'start')
                {
                    var event_name = 'Quarters_View';
                    isActive = 'suku';
                    facebook_pixel_events(event_name);
                }
                
                if (height >= half_screen  && isActive == 'suku') 
                {
                    var event_name = 'Halfs_View';
                    isActive = 'separuh';
                    facebook_pixel_events(event_name);
                }

                if (height >= bottom_screen  && isActive == 'separuh') 
                {
                    var event_name = 'Bottoms_View';
                    isActive = 'end';
                    facebook_pixel_events(event_name);
                }
            }
            );

        </script>


        <?php

        
    }

}
