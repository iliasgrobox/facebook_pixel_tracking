<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking {

    protected $loader;

    // Plugin core class
    public function __construct() {

        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_settings_hooks();
        $this->define_metabox_hooks();
        $this->define_marketer_hooks();
        $this->define_public_hooks();

    }

    // Load the required dependencies for this plugin
    private function load_dependencies() {

        require_once( CARTFLOWS_FB_TRACKING_PATH . 'libraries/meta-box/meta-box.php' );
        require_once( CARTFLOWS_FB_TRACKING_PATH . 'includes/class-cartflows-fb-tracking-loader.php' );
        require_once( CARTFLOWS_FB_TRACKING_PATH . 'admin/class-cartflows-fb-tracking-admin.php' );
        require_once( CARTFLOWS_FB_TRACKING_PATH . 'admin/class-cartflows-fb-tracking-settings.php' );
        require_once( CARTFLOWS_FB_TRACKING_PATH . 'admin/class-cartflows-fb-tracking-metabox.php' );
        require_once( CARTFLOWS_FB_TRACKING_PATH . 'admin/class-cartflows-fb-tracking-marketer.php' );
        require_once( CARTFLOWS_FB_TRACKING_PATH . 'public/class-cartflows-fb-tracking-public.php' );

        $this->loader = new Cartflows_FB_Tracking_Loader();

    }

    // Register all of the hooks for admin
    private function define_admin_hooks() {

        $admin = new Cartflows_FB_Tracking_Admin();
        $this->loader->add_action( 'admin_notices', $admin, 'cartflows_notice' );

    }

    // Register all of the hooks for settings
    private function define_settings_hooks() {

        $settings = new Cartflows_FB_Tracking_Settings();

        $this->loader->add_action( 'admin_menu', $settings, 'register_menu' );
        $this->loader->add_action( 'admin_post_cartflows_fb_tracking_settings', $settings, 'save_settings' );

    }

    // Register all of the hooks for metabox
    private function define_metabox_hooks() {

        $metabox = new Cartflows_FB_Tracking_Metabox();

        $this->loader->add_filter( 'rwmb_meta_boxes', $metabox, 'register_metabox_flow_page' );
        $this->loader->add_filter( 'rwmb_meta_boxes', $metabox, 'register_metabox_step_page' );
        $this->loader->add_action( 'admin_print_styles', $metabox, 'print_styles' );

    }

    // Register all of the hooks for marketer
    private function define_marketer_hooks() {

        $marketer = new Cartflows_FB_Tracking_Marketer();
        $this->loader->add_filter( 'woocommerce_prevent_admin_access', $marketer, 'allow_admin_access' );

    }

    // Register all of the hooks for public
    private function define_public_hooks() {

        $public = new Cartflows_FB_Tracking_Public();

        $this->loader->add_filter( 'global_cartflows_js_localize', $public, 'override_cartflows_facebook_settings' );
        $this->loader->add_action( 'wp_head', $public, 'print_fb_tracking_script', 20 ); // Load after Cartflows Facebook pixel code

    }

    // Run the loader to execute all of the hooks with WordPress
    public function run() {
        $this->loader->run();
    }

}
