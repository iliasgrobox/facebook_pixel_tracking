<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking_Activator {

    // Fired during plugin activation.
    public static function activate() {

        // Register marketer role
        $marketer = add_role(
            'marketer',
            __( 'Marketer', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
            array(
                'read' => true,
                'manage_cartflows_fb_tracking_settings' => true,
            )
        );

    }

}
