=== Facebook Tracking for Cartflows ===

== Description ==

Enable marketer to assign specified Facebook tracking ID for their CartFlows landing page.

== Features ==

- Admin able to register new marketer.
- Admin able to assign CartFlows landing page to specified marketer.
- Admin able to specify Facebook tracking event for flow in CartFlows landing page.
- Marketer able to view CartFlows landing page assigned to them.
- Marketer able to specify Facebook tracking ID to their CartFlows landing page.

== Installation ==

1. Log in to your WordPress admin.
2. Navigate to "Plugins" > "Add New" in the sidebar and upload "cartflows-fb-tracking.zip".
3. Activate the plugin.
4. Navigate to "Cartflows" in the sidebar and access the Cartflows settings page.
5. Scroll to "Facebook Pixel Settings" section and check "Enable Facebook Pixel Tracking".
6. Update the Cartflow settings.

== How to Use? ==

1. For admin
    i. Register marketer
       a. Navigate to "Users" > "Add New" in the sidebar.
       b. Fill in user data. For user role, select "Marketer".
       c. Click "Add New User" button and done.

   ii. Assign marketer and specify tracking event.
       a. Navigate to "Cartflows" > "Flows" in the sidebar.
       b. Select flow and Click edit.
       c. Assign marketer.
       d. Click edit on specified flow step.
       e. Enter event for Facebook pixel tracking.
       f. Click "Update" button and done.

2. For marketer
   a. Navigate to "Facebook Tracking for Cartflows" in the sidebar.
   b. Enter Facebook pixel tracking ID for flow assigned by admin.
   c. Click "Save Changes" button and done.

== Changelog ==

= 1.0.0 =
- Initial release of the plugin.
