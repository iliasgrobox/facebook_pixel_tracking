<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking_Settings {

    private $id = 'cartflows_fb_tracking';
    private $menu_slug = 'cartflows-fb-tracking';

    // Register tracking settings page menu
    public function register_menu() {

        $title = __( 'Facebook Tracking for Cartflows', CARTFLOWS_FB_TRACKING_TEXTDOMAIN );

        add_menu_page( 
            $title, 
            $title,
            'manage_cartflows_fb_tracking_settings',
            $this->menu_slug,
            array( $this, 'render_form' ),
            'dashicons-tag'
        );

    }

    // Render tracking settings form
    public function render_form() {

        // Check if current user can update flow and tracking
        if ( !current_user_can( 'manage_cartflows_fb_tracking_settings' ) ) {
            return;
        }

        // Get flow assigned to current user (marketer)
        $flows = get_posts( array(
            'numberposts' => -1,
            'post_type'  => 'cartflows_flow',
            'meta_key'   => 'cartflows_fb_tracking_marketer',
            'meta_value' => get_current_user_id(),
        ) );

        ob_start();
        include( CARTFLOWS_FB_TRACKING_PATH . 'admin/views/tracking-settings-form.php' );
        echo ob_get_clean();

    }

    // Handle form submission
    public function save_settings() {

        // Check if current user can update flow and tracking
        if ( !current_user_can( 'manage_cartflows_fb_tracking_settings' ) ) {
            wp_die( __( 'Sorry, you do not have permission to update Facebook Tracking for Cartflows settings.', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ) );
        }

        // Verify nonce
        if (
            !isset( $_POST['cartflows_fb_tracking_settings_nonce'] )
            || !wp_verify_nonce( $_POST['cartflows_fb_tracking_settings_nonce'], 'cartflows_fb_tracking_settings' )
        ) {
            wp_die( __( 'Invalid nonce specified.', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ) );
        }

        // Tracking input ID to be stored in the database
        $tracking_input_id = $this->id . '_flow_tracking';

        // Get flow and tracking ID
        if ( isset( $_POST[ $tracking_input_id ] ) ) {
            $flow_tracking = $_POST[ $tracking_input_id ];

            foreach ( $flow_tracking as $flow_id => $tracking_id ) {
                // Sanitize the input
                $flow_id = sanitize_text_field( $flow_id );
                $tracking_id = sanitize_text_field( $tracking_id );

                // Update Facebook pixel tracking to specified flow (only for displaying the value on the settings page)
                update_post_meta( $flow_id, $tracking_input_id, $tracking_id );

                // Get the steps under this specified flow
                $steps = get_post_meta( $flow_id, 'wcf-steps', true );

                // Update Facebook pixel tracking to all steps for specified flow
                foreach ( $steps as $step ) {
                    if ( isset( $step['id'] ) && !empty( $step['id'] ) ) {
                        update_post_meta( $step['id'], $tracking_input_id, $tracking_id );
                    }
                }
            }
        }

        // Redirect back with success notice
        wp_safe_redirect( admin_url( 'admin.php?page=' . $this->menu_slug . '&message=saved' ) );
        exit;

    }

}
