<?php if ( !defined( 'ABSPATH' ) ) exit; ?>

<div class="wrap">
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

    <?php if ( isset( $_GET['message'] ) == 'saved' ) : ?>
        <div class="notice notice-success">
            <p><?php esc_html_e( 'Settings saved successfully.', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ); ?></p>
        </div>
    <?php endif; ?>

    <form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">
        <input type="hidden" name="action" value="cartflows_fb_tracking_settings">

        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th><strong><?php esc_html_e( 'Flow', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ); ?></strong></th>
                    <th><strong><?php esc_html_e( 'Facebook Pixel Tracking ID', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ); ?></strong></th>
                </tr>
            </thead>
            <tbody>
                <?php
                wp_nonce_field( 'cartflows_fb_tracking_settings', 'cartflows_fb_tracking_settings_nonce' );

                if ( !empty( $flows ) ) :
                    foreach ( $flows as $flow ) :
                    ?>
                        <tr>
                            <td>
                                <label for="<?php echo esc_attr( $this->id . '_flow_tracking_' . $flow->ID ); ?>">
                                    <?php echo esc_html( $flow->post_title ); ?>
                                </label>
                            </td>
                            <td>
                                <input
                                    type="number"
                                    id="<?php echo esc_attr( $this->id . '_flow_tracking_' . $flow->ID ); ?>"
                                    name="<?php echo esc_attr( $this->id . '_flow_tracking[' . $flow->ID . ']' ); ?>"
                                    style="width: 100%;"
                                    value="<?php echo get_post_meta( $flow->ID, $this->id . '_flow_tracking', true ); ?>"
                                >
                            </td>
                        </tr>
                    <?php
                    endforeach;
                else :
                ?>
                    <tr>
                        <td colspan="2">
                            <?php esc_html_e( 'No flow assigned.', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ); ?>
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>

        <?php submit_button(); ?>
    </form>
</div>
