<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking_Admin {

    // Show notice if Cartflows not installed
    public function cartflows_notice() {

        if ( !$this->is_cartflows_activated() ) {
            $plugin = __( 'Facebook Tracking for Cartflows', CARTFLOWS_FB_TRACKING_TEXTDOMAIN );
            $message = __( 'Cartflows needs to be installed and activated.', CARTFLOWS_FB_TRACKING_TEXTDOMAIN );

            printf( '<div class="notice notice-error"><p><strong>%1$s:</strong> %2$s</p></div>', $plugin, $message );
        }

    }

    // Check if Cartflows is installed and activated
    private function is_cartflows_activated() {
        return in_array( 'cartflows/cartflows.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) );
    }

}
