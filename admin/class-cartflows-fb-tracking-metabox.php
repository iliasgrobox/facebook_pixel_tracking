<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking_Metabox {

    private $id = 'cartflows_fb_tracking';

    // Register metabox for Cartflows flow page
    public function register_metabox_flow_page( $metaboxes ) {

        $metaboxes[] = array(
            'title'      => esc_html__( 'Facebook Tracking for Cartflows', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
            'id'         => $this->id,
            'post_types' => 'cartflows_flow',
            'context'    => 'side',
            'priority'   => 'high',
            'fields'     => array(
                array(
                    'type'       => 'user',
                    'name'       => esc_html__( 'Marketer', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
                    'id'         => $this->id . '_marketer',
                    'desc'       => esc_html__( 'Assign marketer to this flow', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
                    'field_type' => 'select_advanced',
                    'query_args' => array(
                        'role' => 'marketer',
                    ),
                ),
            ),
        );

        return $metaboxes;

    }

    // Register metabox for Cartflows step page
    public function register_metabox_step_page( $metaboxes ) {

        // Facebook pixel events reference page
        $reference_url  = 'https://developers.facebook.com/docs/facebook-pixel/reference#standard-events';
        $reference_link = sprintf( '<a href="%1$s" target="_blank">%1$s</a>', $reference_url );

        // Cartflows settings page
        $cartflow_settings_url   = admin_url( 'admin.php?page=cartflows_settings' );
        $cartflow_settings_label = __( 'Cartflow settings page', CARTFLOWS_FB_TRACKING_TEXTDOMAIN );
        $cartflow_settings_link  = sprintf( '<a href="%s" target="_blank">%s</a>', $cartflow_settings_url, $cartflow_settings_label );

        $metaboxes[] = array(
            'title'      => esc_html__( 'Facebook Tracking for Cartflows', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
            'id'         => $this->id,
            'post_types' => 'cartflows_step',
            'context'    => 'normal',
            'priority'   => 'high',
            'fields'     => array(
                array(
                    'type'        => 'text',
                    'name'        => esc_html__( 'Pixel Tracking Events', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
                    'id'          => $this->id . '_event',
                    'desc'        => sprintf( __( 'Enter Facebook Pixel tracking event. Refer %s<br>For conversion tracking, please configure on %s under Facebook Pixel Settings section.', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ), $reference_link, $cartflow_settings_link ),
                    'placeholder' => esc_html__( 'Eg: ViewContent', CARTFLOWS_FB_TRACKING_TEXTDOMAIN ),
                ),
            ),
        );

        return $metaboxes;

    }

    // Print metabox styles
    public function print_styles() {

        global $pagenow, $post;

        // Only print custom styles on cartflows step editor page
        if (
            ( $pagenow !== 'post-new.php' || $pagenow !== 'post.php' )
            && ( isset( $post->post_type ) && $post->post_type !== 'cartflows_step' )
        ) return;
        ?>
        <style type="text/css">
            #cartflows_fb_tracking {
                border: 1px solid #ccd0d4;
                margin-bottom: 30px;
            }
            #cartflows_fb_tracking .inside {
                padding-bottom: 18px;
                margin-top: 15px !important;
            }
        </style>
        <?php
    }

}
