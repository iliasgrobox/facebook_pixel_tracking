<?php
if ( !defined( 'ABSPATH' ) ) exit;

class Cartflows_FB_Tracking_Marketer {

    // Prevent WooCommerce from redirecting to my account page for marketers
    public function allow_admin_access( $prevent_access ) {

        if ( current_user_can( 'manage_cartflows_fb_tracking_settings' ) ) {
            return false; // True = prevent access, false = allow access
        }

        return $prevent_access;

    }

}
