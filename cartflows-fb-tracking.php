<?php
/**
 * Plugin Name: Facebook Tracking for Cartflows
 * Description: Enable marketer to assign specified Facebook tracking ID for their CartFlows landing page and detect scrolling pixel.
 * Version:     1.0.4
 * Author:      Yied Pozi
 * Author URI:  https://yiedpozi.my/
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/iliasgrobox/facebook_pixel_tracking/',
	__FILE__,
	'facebook_pixel_tracking'
);

$myUpdateChecker->setBranch('main');

if ( !defined( 'ABSPATH' ) ) exit;

define( 'CARTFLOWS_FB_TRACKING_FILE', __FILE__ );
define( 'CARTFLOWS_FB_TRACKING_PATH', plugin_dir_path( CARTFLOWS_FB_TRACKING_FILE ) );
define( 'CARTFLOWS_FB_TRACKING_TEXTDOMAIN', 'cartflows-fb-tracking' );

// Run during plugin activation
function activate_cartflows_fb_tracking() {
    require_once( CARTFLOWS_FB_TRACKING_PATH . 'includes/class-cartflows-fb-tracking-activator.php' );
    Cartflows_FB_Tracking_Activator::activate();
}
register_activation_hook( __FILE__, 'activate_cartflows_fb_tracking' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks
 */
require( CARTFLOWS_FB_TRACKING_PATH . 'includes/class-cartflows-fb-tracking.php' );

function run_cartflows_fb_tracking() {
    $plugin = new Cartflows_FB_Tracking();
    $plugin->run();
}
run_cartflows_fb_tracking();
